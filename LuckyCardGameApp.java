import java.util.Scanner;
public class LuckyCardGameApp{
	public static void main(String[] args){
		
		Deck deckObject = new Deck();
		
		deckObject.shuffle();
		
		Scanner reader = new Scanner(System.in);
		System.out.println("Hello User! Welcome to the Card Game :D Please insert how many cards you wish to remove");
		int userRemove = reader.nextInt();
		System.out.println("This is the initial number of cards " + deckObject.length());
		int newNumCards = deckObject.length() - userRemove;
		System.out.println("After removing " + userRemove + " cards, you now have " + newNumCards + " cards");
		deckObject.shuffle();
		System.out.println(deckObject);
	
	}

}