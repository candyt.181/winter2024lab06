public class Card{
	//fields(2)
	private String suit;
	private String value;
	
	//constructor
	public Card(String suit, String value){
		this.suit = suit;
		this.value = value;
	}
	
	//getters 
	public String getSuit(){
		return this.suit;
	}
	
	public String getValue(){
		return this.value;
	}
	
	//toString() method
	public String toString(){
		return this.value + " of " + this.suit;
	}
}