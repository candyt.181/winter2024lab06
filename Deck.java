import java.util.Random;
public class Deck{
	//Fields
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	
	//constructor
	public Deck(){
		this.numberOfCards = 52;
		this.rng = new Random();
		this.cards = new Card[52];
		
		String[] suit = {"Hearts", "Diamonds", "Spades", "Clubs"};
		String[] values = {"Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"};
		int count = 0;
		for(int j = count; j <suit.length; j++){
			for(int i=0; i<values.length; i++){
					this.cards[count] = new Card(suit[j], values[i]);
					count++;
			}
		}
		
	}

	public int length(){
		return this.cards.length;
	}
	
	public Card drawTopCard(){
		numberOfCards = numberOfCards - 1;
		return cards[cards.length-1];
	}
	
	public String toString(){
		String result = "";
		for(int i =0; i<this.cards.length; i++){
			result += this.cards[i] + "\n";
		}
		return result;
	}
	
	public void shuffle(){
		for(int i=0; i<cards.length; i++){
			int number = this.rng.nextInt(this.cards.length);
			Card card = this.cards[i];
			this.cards[i] = this.cards[number];
			this.cards[number] = card;
	
		}
	}
}